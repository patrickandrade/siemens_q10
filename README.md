## Question 10 for Siemens Programming skills and PL/SQL

This repository holds code for the tenth question of Siemens Programming skills and PL/SQL.

The code consists of a program that calculates the length of a string informed by the user
(without use any pre-existing function or method for this, such as len(), count(), strlen() or lenght());