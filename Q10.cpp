/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <string>
#include <iostream>
#include <stdint.h>

using namespace std;
// Function that returns a length of a string
int StrSize (string s){
    int i=0;
    // Count until end of string (null character)
    while(s[i]){
        i++;
    }
    return i;
}

int main()
{
    string s;
    cout << "Please enter a string: ";
    getline (cin, s);
    cout << endl << "The length of the entered string is: " << StrSize(s);

    return 0;
}